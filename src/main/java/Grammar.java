import java.util.*;

public class Grammar {
    private ArrayList<Rule> rules = new ArrayList<Rule>();
    private ArrayList<String> terminals = new ArrayList<String>();
    private ArrayList<String> nonTerminals = new ArrayList<String>();
    private String startTerminal;

    public void addRule(Rule r) {
        rules.add(r);
    }

    public boolean isLLOneGrammar() {

        /*
         * Возвращает true если является LL(1)-грамматикой
         * */

        for (String t : terminals) {
            ArrayList<ArrayList<String>> firstSets = new ArrayList<ArrayList<String>>();
            for (String trans : getByLeftPart(t)) {
                firstSets.add(removeE(getFirst(trans)));
            }
            if (areIntersecting(firstSets)) {
                System.out.println("First condition not fulfilled");
                return false;
            }
        }
        System.out.println("First condition fulfilled");
        String leftPart;
        if ((leftPart = containsE()) != null) {
            ArrayList<String> leftParts = getByRightPart(leftPart);
            for (String s1 : leftParts) {
                for (String s2 : getByLeftPart(s1)) {
                    if (!s2.equals(leftPart)) {
                        if (areIntersecting(getFirst(s2), getFollow(s1))) {
                            System.out.println("Second condition not fulfilled");
                            return false;
                        }
                    }
                }
            }
        }
        System.out.println("Second condition fulfilled");
        return true;
    }

    public void setTerminals(ArrayList<String> terminals) {
        this.terminals = terminals;
    }

    public void setNonTerminals(ArrayList<String> nonTerminals) {
        this.nonTerminals = nonTerminals;
    }

    public ArrayList<String> getFirst(String chain) {

        /*
         * Возвраащет множество FIRST(chain)
         * */

        ArrayList<String> prevFirstSet, firstSet = new ArrayList<String>();
        if (chain.equals("")) return firstSet;
        char firstSymbol = chain.toCharArray()[0];
        if (isTerminal(firstSymbol) && chain.length() > 1) {
            return getFirst(String.valueOf(firstSymbol));
        } else if ((('e' == firstSymbol) && (chain.length() == 1)) || (isNonTerminal(firstSymbol))) {
            firstSet.add(String.valueOf(firstSymbol));
            return firstSet;
        }

        boolean changed = true;
        while (changed) {
            changed = false;
            for (String s : getByLeftPart(chain)) {
                prevFirstSet = new ArrayList<String>(firstSet);
                ArrayList<String> first = getFirst(s);
                firstSet = intersectSets(firstSet, first);
                if (!equalSets(prevFirstSet, firstSet)) {
                    changed = true;
                }
            }
        }
        return firstSet;
    }

    public ArrayList<String> getFollow(String chain) {

        /*
         * Возвращает множество FOLLOW(chain)
         * */

        HashMap<String, ArrayList<String>> prevFollowSets, followSets = new HashMap<String, ArrayList<String>>();
        for (String t : terminals) {
            if (t.equals(startTerminal)) {
                ArrayList<String> arrayList = new ArrayList<String>();
                arrayList.add("$");
                followSets.put(t, arrayList);
            } else {
                followSets.put(t, new ArrayList<String>());
            }
        }

        boolean changed = true;
        while (changed) {
            changed = false;
            for (Rule r : rules) {
                String leftPart = r.getLeftPart();
                String rightPart = r.getRightPart();
                ArrayList<String> terminalSet = containsTerminal(rightPart);

                if (!terminalSet.isEmpty()) {
                    for (String terminal : terminalSet) {
                        prevFollowSets = new HashMap<String, ArrayList<String>>(followSets);
                        String before = getBeforeTerminal(rightPart, terminal.toCharArray()[0]);
                        String after = getAfterTerminal(rightPart, terminal.toCharArray()[0]);

                        if (!before.equals("")) {
                            if (after.equals("") || getFirst(after).contains("e")) {
                                followSets.put(terminal, intersectSets(followSets.get(terminal), followSets.get(leftPart)));
                            }
                            ArrayList<String> first = removeE(getFirst(after));
                            followSets.put(terminal, intersectSets(followSets.get(terminal), first));
                            if (!equalSets(prevFollowSets.get(terminal), followSets.get(terminal))) {
                                changed = true;
                            }
                        }
                    }
                }
            }
        }
        System.out.println(followSets);
        return followSets.get(chain);
    }

    public ArrayList<String> containsTerminal(String chain) {

        /*
         * Возвращает все терминалы, которые содержатся в цепочке chain
         * */

        ArrayList<String> terminals = new ArrayList<String>();
        for (Character c : chain.toCharArray()) {
            if (isTerminal(c)) {
                terminals.add(String.valueOf(c));
            }
        }
        return terminals;
    }

    public String getBeforeTerminal(String chain, char t) {

        /*
         * Возвращает цепочку символов перед терминалом t в цепочке chain
         * */

        String partBefore = "";
        for (Character c : chain.toCharArray()) {
            if (!c.equals(t)) {
                partBefore += c;
            } else {
                return partBefore;
            }
        }
        return partBefore;
    }

    public String getAfterTerminal(String chain, char t) {

        /*
         * Возвращает цепочку символов после терминала t в цепочке chain
         * */

        String partAfter = "";
        char[] symbols = chain.toCharArray();
        for (int i = 0; i < symbols.length; i++) {
            if (symbols[i] == t) {
                return String.copyValueOf(symbols, i + 1, symbols.length - i - 1);
            }
        }
        return "";
    }

    public boolean equalSets(ArrayList<String> s1, ArrayList<String> s2) {

        /*
         * Возвращает true если списки эквиваленты
         * */

        Collections.sort(s1);
        Collections.sort(s2);
        return s1.equals(s2);
    }

    public ArrayList<String> getByLeftPart(String leftPart) {

        /* Возвращает массив правых частей правил из грамматики по левой leftPart */

        ArrayList<String> transitions = new ArrayList<String>();
        for (Rule r : rules) {
            if (r.getLeftPart().equals(leftPart)) {
                transitions.add(r.getRightPart());
            }
        }
        return transitions;
    }

    public ArrayList<String> getByRightPart(String rightPart) {

        /* Возвращает массив левых частей правил из грамматики по правой rightPart */

        ArrayList<String> transitions = new ArrayList<String>();
        for (Rule r : rules) {
            if (r.getRightPart().equals(rightPart)) {
                transitions.add(r.getLeftPart());
            }
        }
        return transitions;
    }

    public ArrayList<String> intersectSets(ArrayList<String> s1, ArrayList<String> s2) {

        /* Возвращает объединение двух множеств */

        ArrayList<String> newSet = new ArrayList<String>(s1);
        for (String s : s2) {
            if (!newSet.contains(s)) {
                newSet.add(s);
            }
        }
        return newSet;
    }

    public ArrayList<String> removeE(ArrayList<String> list) {

        /* Удаляет из множества символ e*/

        ArrayList<String> newList = new ArrayList<String>();
        for (String s : list) {
            if (!s.equals("e")) {
                newList.add(s);
            }
        }
        return newList;
    }

    public String containsE() {

        /* Возвращает все левые части правил вида A->e */

        for (Rule r : rules) {
            if (r.getRightPart().equals("e")) {
                return r.getLeftPart();
            }
        }
        return null;
    }

    public boolean isTerminal(char s) {

        /*Возвращает true, если символ s терминальный*/

        for (String t : terminals) {
            if (t.equals(String.valueOf(s))) {
                return true;
            }
        }
        return false;
    }

    public boolean isNonTerminal(char s) {

        /* Возвращает true, если символ s нетерминальный*/

        for (String n : nonTerminals) {
            if (n.equals(String.valueOf(s))) {
                return true;
            }
        }
        return false;
    }

    public void setStartTerminal(String startTerminal) {
        this.startTerminal = startTerminal;
    }

    public boolean areIntersecting(ArrayList<ArrayList<String>> sets) {

        /* Вовзращает true если массивы попарно пересекаются */

        for (int i = 0; i < sets.size(); i++) {
            for (int j = i + 1; j < sets.size(); j++) {

                if (areIntersecting(sets.get(i), sets.get(j))) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean areIntersecting(ArrayList<String> t1, ArrayList<String> t2) {

        /* Вовзращает true если два массива пересекаются */

        for (String s : t1) {
            if (t2.contains(s)) {
                System.out.println(t1 + " intersects " + t2);
                return true;
            }
        }
        System.out.println(t1 + " does not intersects " + t2);
        return false;
    }
}
