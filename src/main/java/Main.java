import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

public class Main {

    public static final String FILEPATH = "src/main/resources/grammar.txt";
    public static final String FILEPATH2 = "src/main/resources/grammar2.txt";

    public static void main(String[] args) {
        File grammarFile = new File(FILEPATH);
        FileParser fileParser = new FileParser();
        Grammar grammar = fileParser.parseFile(grammarFile);
        String[] nonTerminals = {"a", "b", "c"}, terminals = {"A", "B", "C", "S"};
        String startTerminal = "S";
        grammar.setNonTerminals(new ArrayList<String>(Arrays.asList(nonTerminals)));
        grammar.setTerminals(new ArrayList<String>(Arrays.asList(terminals)));
        grammar.setStartTerminal(startTerminal);
        if (grammar.isLLOneGrammar()) {
            System.out.println("YES. This grammar is LL(1)");
        } else {
            System.out.println("NO. This grammar is not LL(1)");
        }

        System.out.println("\n================\n");

        File grammarFile2 = new File(FILEPATH2);
        Grammar grammar2 = fileParser.parseFile(grammarFile);
        String[] nonTerminals2 = {"a", "b"}, terminals2 = {"A", "B", "S"};
        String startTerminal2 = "S";
        grammar.setNonTerminals(new ArrayList<String>(Arrays.asList(nonTerminals2)));
        grammar.setTerminals(new ArrayList<String>(Arrays.asList(terminals2)));
        grammar.setStartTerminal(startTerminal2);
        if (grammar.isLLOneGrammar()) {
            System.out.println("YES. This grammar is LL(1)");
        } else {
            System.out.println("NO. This grammar is not LL(1)");
        }
    }
}
